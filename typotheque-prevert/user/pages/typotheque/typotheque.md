---
title: Typothèque
custom:
    archivezip: {  }
    titresvg: {  }
    asciiart:
        -
            ascii: "  _   ,_,   _\n / `'=) (='` \\\n/.-.-.\\ /.-.-.\\ \n`      \"      `"
        -
            ascii: " ,_,\n(.,.)\n(   )\n-\"-\"-----"
        -
            ascii: " ,_,\n(O,O)\n(   )\n-\"-\"------"
        -
            ascii: "  _      _      _\n>(.)__ <(.)__ =(.)__\n (___/  (___/  (___/  "
        -
            ascii: " __\n( o>\n///\\ \n\\V_/_"
        -
            ascii: "    |\\__/,|   (`\\\n  _.|o o  |_   ) )\n-(((---(((--------"
        -
            ascii: "        (__)    \n`\\------(oo)\n  ||    (__)\n  ||w--||   "
        -
            ascii: "                                        __\n(______________________________________()'`;\n/,                                       /`\n\\\\\"-------------------------------------\\\\\n"
        -
            ascii: "      __\n(____()'`;\n/,     /`\n\\\\\"---\\\\"
        -
            ascii: "     __\n .--()°'.'\n'|, . ,'\n !_-(_\\"
        -
            ascii: "    _    _\n   /=\\\"\"/=\\\n  (=(0_0 |=)__\n   \\_\\ _/_/   )\n     /_/   _  /\\\n    |/ |\\ || |\n"
        -
            ascii: " __v_\n(____\\/{"
        -
            ascii: "       O  o\n  _\\_   o\n\\\\/  o\\ .\n//\\___=\n  ''"
        -
            ascii: "       .\n      \":\"\n    ___:____     |\"\\/\"|\n  ,'        `.    \\  /\n  |  O        \\___/  |\n~^~^~^~^~^~^~^~^~^~^~^~^~\n"
        -
            ascii: "      .            \n\\_____)\\_____\n/--v____ __`<         \n        )/           \n        '"
        -
            ascii: "        _   _\n       (.)_(.)\n    _ (   _   ) _\n   / \\/`-----'\\/ \\\n __\\ ( (     ) ) /__\n )   /\\ \\._./ /\\   (\n  )_/ /|\\   /|\\ \\_(\n"
        -
            ascii: "        ,--,\n  _ ___/ /\\|\n ;( )__, )\n; //   '--;\n  \\     |\n   ^    ^\n"
        -
            ascii: "    \\\\_//\n   __/\".\n  /__ |\n  || ||"
        -
            ascii: "  .|||||||||.   \n |||||||||||||     \n|||||||||||' .\\    \n`||||||||||_,__o   "
        -
            ascii: "     \\\\\n      \\\\_\n      ( _\\\n      / \\__\n     / _/`\"`\n    {\\  )_\n      `\"\"\"`"
        -
            ascii: " ||  ||  \n \\\\()// \n//(__)\\\\\n||    ||"
        -
            ascii: " /@\\ \n`-\\ \\  ______ \n   \\ \\/ ` /  \\      _\n    \\_i / \\  |\\____//\n      | |==| |=----/\n------hn/--hn/--------"
        -
            ascii: "                   ___\n                  / *_) \n       _.----. _ /../\n     /............./\n ___/..(...|.(...|\n/__.-|_|--|_|"
        -
            ascii: " _  _\n(o)(o)--.\n \\../ (  )\n m\\/m--m'`----"
        -
            ascii: "      .--.\n    .'_\\/_'.\n    '. /\\ .'\n      \"||\"\n       || /\\\n    /\\ ||//\\)\n   (/\\\\||/\n______\\||/_______\n"
        -
            ascii: "       wWWWw   \n vVVVv (___) wWWWw  \n (___)  ~Y~  (___) \n  ~Y~   \\|    ~Y~  \n  \\|   \\ |/   \\| /  \n \\\\|// \\\\|// \\\\|/// \n^^^^^^^^^^^^^^^^^^^^"
        -
            ascii: "  .-\"\"\"-.\n /* * * *\\\n:_.-:`:-._;\n    (_)\n \\|/(_)\\|/"
        -
            ascii: "\n ,xXXXXx,\n,XXXXXXXX,\nXXXXXXXXXX\n`\"\"\"XX\"\"\"`\n    XX\n    XX\n    XX"
        -
            ascii: "           _\n       _ (`-`) _\n     /` '.\\ /.' `\\\n     ``'-.,=,.-'``\n       .'//v\\\\'.\n      (_/\\ \" /\\_)\n          '-'"
        -
            ascii: "+====+\n|(::)|\n| )( |\n|(..)|\n+====+"
        -
            ascii: "   _____\n  / .  /\\\n /____/..\\\n \\'  '\\  /\n  \\'__'\\/"
---

Les étudiant·e·s du Lycée Jacques Prévert créé·e·s des typographies. Nos camarades dessinent des lettres, impriment des messages, examinent des caractères, jouent avec des balises et codent des mots.

**Licence**: Site sous GNU GPL et Typographies sous SIL.
Le site et son contenu sont libres d'utilisations, de modifications, et de redistributions si les auteur·e·s sont crédité·e·s.

**Contact**: blabla@lyceejacquesprevert.net
ㅤ
**Accompagnateur et Développeur**: le collectif Bonjour Monde.
**Membres Typothèque de l'ÉSAC**: Gabin, Jérémy, Ines, Grégoriane, Helmi, Laureline.
**Participants au Workshop**: Paul, Eugénie, Valentin, Chici, Sarah.
**Remerciements aux profeseurs**: Romuald.