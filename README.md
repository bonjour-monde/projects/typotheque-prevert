# Typothèque-Prévert

Repertoire de travail pour le workshop de création d'une typothèque pour le DNMADE du lycée Jacques Prévert ( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)



**Liens intéressants !**

[Fonderie Emigre](https://www.emigre.com/)

[Henri Michaux](https://fr.wikipedia.org/wiki/Henri_Michaux)

[Typographie Michaux](http://benjamindumond.fr/etc/etc/guidemichaux.pdf)

[Memoire Idée de caractères](http://benjamindumond.fr/etc/ressources/Benjamin_Dumond_-_Idees_de_caracteres.pdf)

[Stratégies Italiques](http://www.strategiesitaliques.fr/)

[Énochien](https://fr.wikipedia.org/wiki/%C3%89nochien)

[Site sur Magie et Design](http://conjonction.org/)

[,.:⸨ 𝕲𝖗𝖎𝖋𝖎 ⸩:.,](https://grifi.fr/fr)

[Projet Jester](https://jester.grifi.fr/)

[Seam Carving Demo](https://www.aryan.app/seam-carving/)

[L'écriture asémique](https://www.sakartonn.fr/lecriture-asemique/#:~:text=L'%C3%A9criture%20as%C3%A9mique%20(non%2D,une%20totale%20libert%C3%A9%20d'interpr%C3%A9tation.)

[Typothèque Luuse](https://typotheque.luuse.fun/)

[Typothèque Cambrai](https://luuse.gitlab.io/workshops/www.typotheque-esac-cambrai/)

[Plain form](https://plain-form.com/)

[Nan foundry](https://www.nan.xyz/fonts/)

[Fonderie Forge](http://forge.cestainsi.online/)

[Conceptual Display Typefaces — La Bolde Vita](https://www.laboldevita.com/fonts)

[PagedJs](https://pagedjs.org/)